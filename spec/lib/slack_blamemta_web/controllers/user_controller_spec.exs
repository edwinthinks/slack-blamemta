defmodule SlackBlamemtaWeb.UserControllerSpec do
  use ESpec

  use Phoenix.ConnTest
  @endpoint SlackBlamemtaWeb.Endpoint

  describe "create" do
    subject do: get(connection(), "/users/new") 

    let :connection, do: build_conn()

    before do: allow SlackBlamemta.Slack
      |> to(accept :store_user, fn(_a) -> store_user_output() end)

    context "when store user was succesful" do
      let :store_user_output, do: {:ok, %SlackBlamemta.Users.User{}}

      it "should render success text" do
        expect(get_flash(subject(), :info)) |> to(eq "Successly Authenticated!")
      end
    end

    context "when store user was not successful" do
      let :store_user_output, do: {:error, %{errors: fake_error()}}
      let :fake_error, do: "fake-error-msg"

      it "should render the error msg" do
        expect(get_flash(subject(), :error)) |> to(eq "Could not authenticate because: #{fake_error()}")
      end
    end
  end


end

