defmodule SlackBlamemtaWeb.SlackOauthUrlControllerSpec do
  use ESpec

  use Phoenix.ConnTest
  @endpoint SlackBlamemtaWeb.Endpoint

  describe "new" do
    subject do: get(connection(), "/slack-oauth-urls/new") 

    let :connection, do: build_conn()
    let :fake_oauth_url, do: "https://fake-oauth-url"
    before do: allow SlackBlamemta.Slack |> to(accept :get_oauth_url, fn -> fake_oauth_url() end)

    it "should redirect to a slack oauth url" do
      expect redirected_to(subject()) |> to(eq fake_oauth_url())
    end
  end


end
