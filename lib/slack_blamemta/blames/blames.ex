defmodule SlackBlamemta.Blames do
  @moduledoc """
  """

  @doc """
  Returns randomly generated a MTA blame
  """
  def get_blame do
    apology = SlackBlamemta.Blames.get_random_apology()
    mta_excuse = SlackBlamemta.Blames.get_random_mta_excuse()

    "#{apology}, #{mta_excuse}"
  end

  @doc """
  Returns the list of MTA excuses.

  ## Examples

      iex> SlackBlamemta.Blames.list_mta_excuses()
      ["train had a sick passenger", "train line was doing 'signal modernization'", "there was a track fire", "train had signal problems...", "train had a police investigation", "train had service changes", "train had track work being done", "train was held by the train dispatcher", "train traffic..."]
  """
  def list_mta_excuses do
    [
      "train had a sick passenger",
      "train line was doing 'signal modernization'",
      "there was a track fire",
      "train had signal problems...",
      "train had a police investigation",
      "train had service changes",
      "train had track work being done",
      "train was held by the train dispatcher",
      "train traffic..."
    ]
  end


  @doc """
  Returns the a single random MTA excuse
  """
  def get_random_mta_excuse do
    Enum.random(SlackBlamemta.Blames.list_mta_excuses());
  end

  @doc """
  Returns the list of apologies

  ## Examples
      iex> SlackBlamemta.Blames.list_apologies()
      ["Sorry", "Running late", "Gonna be a few minutes late", "On my way", "Not gonna make it", "Can we reschedule the meeting to later", "Be there as fast as I can"]
  """

  def list_apologies do
    [
      "Sorry",
      "Running late",
      "Gonna be a few minutes late",
      "On my way",
      "Not gonna make it",
      "Can we reschedule the meeting to later",
      "Be there as fast as I can"
    ]
  end

  @doc """
  Returns a random apology
  """
  def get_random_apology do
    Enum.random(SlackBlamemta.Blames.list_apologies());
  end

end
