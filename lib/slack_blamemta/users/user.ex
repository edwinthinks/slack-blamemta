defmodule SlackBlamemta.Users.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :slack_user_id, :string
    field :slack_team_id, :string
    field :token, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:slack_user_id, :slack_team_id, :token])
    |> validate_required([:token])
    |> unique_constraint(:unique_user_team, name: :unique_user_team)
  end

end
