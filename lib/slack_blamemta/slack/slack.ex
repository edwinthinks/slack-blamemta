defmodule SlackBlamemta.Slack do
  @moduledoc """
  A module comprising functions that enable the application to access
  slack functionality.
  """

  alias SlackBlamemta.Users
  alias SlackBlamemta.Users.User

  @doc """
  Returns the oauth url used to direct 

  ## Examples
      iex> get_oauth_url()
      "https://slack.com/oauth/authorize?client_id=something&scope=commands&state=random_state"
  """
  def get_oauth_url do
    client_id = slack_client_id()
    state = generate_oauth_url_state()
    scope = generate_oauth_url_scope()

    "https://slack.com/oauth/authorize?client_id=#{client_id}&scope=#{scope}&state=#{state}"
  end

  def post_message(%User{} = user, channel_id, text) do
    opts = %{
      token: user.token,
      as_user: true
    }

    case Slack.Web.Chat.post_message(channel_id, text, opts) do
      %{"ok" => true} ->
        {:ok}
      %{"ok" => false, "error" => error} ->
        {:error, %{errors: error}}
    end
  end

  def store_user(code) do
    with {:ok, access_token} <- get_access_token(code),
         {:ok, user_data} <- get_user_data(access_token),
         {:ok, user} <- Users.create_or_update(user_data) do
      {:ok, user}
    end
  end

  defp get_access_token(code) do
    case Slack.Web.Oauth.access(slack_client_id(), slack_client_secret(), code) do
      %{"ok" => true, "access_token" => access_token} ->
        {:ok, access_token}
      %{"ok" => false, "error" => error} ->
        {:error, %{errors: error}}
    end
  end

  defp get_user_data(access_token) do
    case Slack.Web.Auth.test(%{token: access_token}) do
      %{"ok" => true, "user_id" => user_id, "team_id" => team_id} ->
        {:ok, %{slack_user_id: user_id, slack_team_id: team_id, token: access_token}}
      %{"ok" => false, "error" => error} ->
        {:error, %{errors: error}}
    end
  end

  defp slack_client_id do
    System.get_env("CLIENT_ID")
  end

  defp slack_client_secret do
    System.get_env("CLIENT_SECRET")
  end

  defp generate_oauth_url_state do
    {:ok, token, _} = SlackBlamemta.Token.generate_and_sign()
    token
  end

  defp generate_oauth_url_scope do
    "commands"
  end

end
