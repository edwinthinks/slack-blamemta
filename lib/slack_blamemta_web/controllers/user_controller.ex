defmodule SlackBlamemtaWeb.UserController do
  use SlackBlamemtaWeb, :controller

  def new(conn, params) do
    case SlackBlamemta.Slack.store_user(params["code"]) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Successly Authenticated!")
        |> redirect(to: "/")
      {:error, %{errors: errors}} ->
        conn
        |> put_flash(:error, "Could not authenticate because: #{errors}")
        |> redirect(to: "/")
    end
  end

end
