defmodule SlackBlamemtaWeb.PageController do
  use SlackBlamemtaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
