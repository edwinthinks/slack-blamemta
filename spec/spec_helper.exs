ESpec.configure fn(config) ->
  config.before fn(tags) ->
    {:shared, hello: :world, tags: tags}
    Ecto.Adapters.SQL.Sandbox.checkout(SlackBlamemta.Repo)
  end

  config.finally fn(_shared) ->
    :ok
  end
end
