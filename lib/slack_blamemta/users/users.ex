defmodule SlackBlamemta.Users do

  alias SlackBlamemta.Users.User
  alias SlackBlamemta.Repo

  def create_or_update(data) do
    opts = [
      slack_user_id: data[:slack_user_id], 
      slack_team_id: data[:slack_team_id]
    ]

    case Repo.get_by(User, opts) do
      nil -> User.changeset(%User{}, data)
      user -> user |> User.changeset(data)
    end 
    |> Repo.insert_or_update
  end

  def get_authenticated(team_id, user_id) do
    case Repo.get_by(User, %{slack_team_id: team_id, slack_user_id: user_id}) do
      nil -> 
        {:error, %{errors: "could not find authentication for user"}}
      user ->
        {:ok, user}
    end
  end

end
