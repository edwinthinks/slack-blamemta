defmodule SlackBlamemtaWeb.BlameController do
  use SlackBlamemtaWeb, :controller

  alias SlackBlamemta.Slack
  alias SlackBlamemta.Blames
  alias SlackBlamemta.Users

  def create(conn, %{"channel_id" => channel_id, "team_id" => team_id, "user_id" => user_id}) do
    random_blame = Blames.get_blame()
    
    with {:ok, user} <- Users.get_authenticated(team_id, user_id),
         {:ok} <- Slack.post_message(user, channel_id, random_blame) do
      conn 
      |> put_status(200) 
      |> text("")
    else
      {:error, %{errors: errors}} ->
        conn
        |> put_status(400)
        |> json(%{text: errors})
    end
  end

end
