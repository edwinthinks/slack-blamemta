defmodule SlackBlamemtaWeb.SlackOauthUrlController do
  use SlackBlamemtaWeb, :controller

  def new(conn, _params) do
    oauth_url = SlackBlamemta.Slack.get_oauth_url()
    redirect(conn, external: oauth_url)
  end


end

