defmodule SlackBlamemta.BlamesSpec do
  use ESpec
  doctest SlackBlamemta.Blames

  describe "get_blame" do
    subject(described_module().get_blame)

    let :random_apology, do: "My bad"
    before do: allow SlackBlamemta.Blames |> to(accept :get_random_apology, fn -> random_apology() end)
    let :random_mta_excuse, do: "train had showtime"
    before do: allow SlackBlamemta.Blames |> to(accept :get_random_mta_excuse, fn -> random_mta_excuse() end)

    it "should equal a random apology and random mta excuse" do
      expect(subject()) |> to(eq "#{random_apology()}, #{random_mta_excuse()}")
    end
  end

  describe "list_mta_excuses" do
    subject(described_module().list_mta_excuses)

    it "should equal the expected list of mta excuses" do
      expect(subject()) |> to(match_list [
        "train had a sick passenger",
        "train line was doing 'signal modernization'",
        "there was a track fire",
        "train had signal problems...",
        "train had a police investigation",
        "train had service changes",
        "train had track work being done",
        "train was held by the train dispatcher",
        "train traffic..."
      ])
    end
  end

  describe "get_random_mta_excuse" do
    subject do: described_module().get_random_mta_excuse()

    let :fake_mta_excuses, do: ["excuse_1", "excuse_2"]
    before do: allow SlackBlamemta.Blames |> to(accept :list_mta_excuses, fn -> fake_mta_excuses() end)

    it "should return a single random mta excuse" do
      expect(fake_mta_excuses()) |> to(have subject())
    end
  end

  describe "list_apologies" do
    subject(described_module().list_apologies)

    it "should equal the expected list of apologies" do
      expect(subject()) |> to(match_list [
        "Sorry",
        "Running late",
        "Gonna be a few minutes late",
        "On my way",
        "Not gonna make it",
        "Can we reschedule the meeting to later",
        "Be there as fast as I can"
      ])
    end
  end

  describe "get_random_apology" do
    subject do: described_module().get_random_apology()

    let :fake_apologies, do: ["apology_1", "apology_2"]
    before do: allow SlackBlamemta.Blames |> to(accept :list_apologies, fn -> fake_apologies() end)

    it "should return a single random mta excuse" do
      expect(fake_apologies()) |> to(have subject())
    end
  end

end
