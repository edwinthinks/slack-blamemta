defmodule SlackBlamemta.UserSpec do
  use ESpec

  alias SlackBlamemta.Users.User
  alias SlackBlamemta.Repo

  describe "changeset" do
    subject do: %User{} |> User.changeset(attrs()) |> Repo.insert

    let :attrs, do: %{slack_user_id: slack_user_id(), slack_team_id: slack_team_id(), token: token()}
    let :slack_user_id, do: "a_slack_user_id"
    let :slack_team_id, do: "a_slack_team_id"
    let :token, do: "fake_token"

    context "when given valid attributes" do
      it "should be ok and return a User" do
        {:ok, user} = subject()
        expect(user) |> to(be_struct User)
      end
    end

    context "when given no token" do
      let :token, do: nil

      it "should have an error that says token cannot be blank" do
        {:error, %{errors: errors}} = subject()
        expect(errors) |> to(eq [token: {"can't be blank", [validation: :required]}])
      end
    end

    context "when given a slack_user_id and slack_team_id combination that has been used already" do
      before do
        User.changeset(%User{}, attrs()) |> Repo.insert!
      end

      it "should not be valid" do
        {:error, %{errors: errors}} = subject()
        expect(errors) |> to(eq [unique_user_team: {"has already been taken", [constraint: :unique, constraint_name: "unique_user_team"]}])
      end
    end

  end
end
