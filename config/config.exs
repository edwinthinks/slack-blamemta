# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :slack_blamemta,
  ecto_repos: [SlackBlamemta.Repo]

# Configures the endpoint
config :slack_blamemta, SlackBlamemtaWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "F8tJ9ja2XpVd0fSL6Lx8vrJgAbW4HV136CSHfmYI/AhH/evuSMzx3yUSF2MynL+2",
  render_errors: [view: SlackBlamemtaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SlackBlamemta.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :joken,
  default_signer: "Zij2WScw8Bq/It3sBRFKC4hDc/osq9bAT9p9bS7YUknviu1tRBKhn3BKm1/rZhWD"

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
