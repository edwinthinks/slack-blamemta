defmodule SlackBlamemta.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :slack_user_id, :string
      add :slack_team_id, :string
      add :token, :string, null: false

      timestamps()
    end

    create unique_index(:users, [:slack_user_id, :slack_team_id], name: :unique_user_team)
  end
end
