defmodule SlackBlamemtaWeb.BlameControllerSpec do
  use ESpec

  use Phoenix.ConnTest
  @endpoint SlackBlamemtaWeb.Endpoint

  alias SlackBlamemta.Users.User  

  describe "create" do
    subject do: post(connection(), "/api/blames", %{team_id: team_id(), user_id: user_id(), channel_id: channel_id()})

    let :connection, do: build_conn(:post, "/blames")
    let :team_id, do: "fake_team_id"
    let :user_id, do: "fake_user_id"
    let :channel_id, do: "fake_channel_id"

    context "when the operations are succesful" do

      before do
        allow SlackBlamemta.Users 
        |> to(accept :get_authenticated, fn(_a,_b) -> {:ok, %User{}} end)

        allow SlackBlamemta.Slack 
        |> to(accept :post_message, fn(_a,_b,_c) -> {:ok} end)
      end

      it "should have a status code of 200" do
        expect(subject().status) |> to(eq 200)
      end

      it "should not have anything in the body" do
        expect(subject().resp_body) |> to(eq "")
      end
    end

    context "when it fails to find a user" do

      let :fake_error_msg, do: "fake_error_msg"

      before do
        allow SlackBlamemta.Users 
        |> to(accept :get_authenticated, fn(_a,_b) -> {:error, %{errors: fake_error_msg()}} end)
      end

      it "should have a status code of 400" do
        expect(subject().status) |> to(eq 400)
      end

      it "should have a json with the error" do
        expect(subject().resp_body) |> to(eq "{\"text\":\"#{fake_error_msg()}\"}")
      end

    end
  end
end


