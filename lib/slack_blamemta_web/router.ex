defmodule SlackBlamemtaWeb.Router do
  use SlackBlamemtaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", SlackBlamemtaWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/slack-oauth-urls", SlackOauthUrlController, only: [:new]
    resources "/users", UserController, only: [:new]
  end

  # Other scopes may use custom stacks.
  scope "/api", SlackBlamemtaWeb do
    pipe_through :api

    resources "/blames", BlameController, only: [:create]
  end
end
