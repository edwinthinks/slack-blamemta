defmodule SlackBlamemta.UsersSpec do
  use ESpec

  alias SlackBlamemta.Repo
  alias SlackBlamemta.Users.User

  describe "create_or_update/1" do
    subject do: described_module().create_or_update(data())

    let :data, do: %{slack_user_id: slack_user_id(), slack_team_id: slack_team_id(), token: token()}
    let :slack_user_id, do: "fake_slack_user_id"
    let :slack_team_id, do: "fake_slack_team_id"
    let :token, do: "token"

    context "when there is no User with the slack_user_id and slack_team_id" do

      before do
        expect(Repo.get_by(User, slack_user_id: slack_user_id(), slack_team_id: slack_team_id())) |> to(eq nil)
      end

      it "should create a User with them" do
        subject()
        expect(Repo.get_by(User, slack_user_id: slack_user_id(), slack_team_id: slack_team_id())) |> to_not(eq nil)
      end

      it "should match ok and user" do
        {msg, user} = subject()
        expect(msg) |> to(eq :ok)
        expect(user) |> to(be_struct User)
      end
    end

    context "when there is a User with the slack_user_id and slack_team_id with a different token" do
      let :different_token, do: "different_token"

      before do
        User.changeset(%User{}, Map.merge(data(), %{token: different_token()})) |> Repo.insert!
        expect(Repo.get_by(User, slack_user_id: slack_user_id(), slack_team_id: slack_team_id())) |> to_not(eq nil)
      end

      it "should not create a new User" do
        expect(fn -> subject() end) |> not_to(change fn -> Repo.all(User) |> Enum.count end)
      end

      it "should change the existing User's token" do
        expect(fn -> subject() end) |> to(change fn ->
          Repo.get_by(User, slack_user_id: slack_user_id(), slack_team_id: slack_team_id()).token 
        end, different_token(), token())
      end

      it "should return ok and user" do
        {msg, user} = subject()
        expect(msg) |> to(eq :ok)
        expect(user) |> to(be_struct User)
      end
    end
  end

  describe "get_authenticated/2" do
    subject do: described_module().get_authenticated(team_id(), user_id())

    let :team_id, do: 'fake_team_id'
    let :user_id, do: 'fake_user_id'

    context "when the user could not be found" do
      before do
        allow Repo |> to(accept :get_by, fn(_a,_b) -> nil end)
      end

      it "should return error and error message" do
        {:error, %{errors: errors}} = subject()
        expect(errors) |> to(eq "could not find authentication for user")
      end
    end

    context "when the user could be found" do

      before do
        allow Repo |> to(accept :get_by, fn(_a,_b) -> %User{} end)
      end

      it "should return ok and the user" do
        {:ok, user} = subject()
        expect(user) |> to(be_struct User)
      end
    end
  end

end
