# SlackBlamemta

A slack application born from the frustration of being late to meetings, events, or dates because of MTA issues. This application enables users to generate a late excuse whilst blaming the MTA in part for the lateness.

Application is currently hosted on heroku [here](https://slack-blamemta.herokuapp.com/)

Built using the [Phoenix Framework](https://phoenixframework.org/) and the [Elixir-Slack library](https://github.com/BlakeWilliams/Elixir-Slack).

## Development
To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Testing
This application uses [espec](https://github.com/antonmi/espec) to run tests:
```
MIX_ENV=test mix espec
```

## Feedback

Please create an issue if you'd like to see more excuses or MTA issues served by this application.
