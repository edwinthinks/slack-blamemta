defmodule SlackBlamemta.Repo do
  use Ecto.Repo,
    otp_app: :slack_blamemta,
    adapter: Ecto.Adapters.Postgres
end
