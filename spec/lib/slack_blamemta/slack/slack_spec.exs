defmodule SlackBlamemta.SlackSpec do
  use ESpec

  alias SlackBlamemta.Users.User

  describe "get_oauth_url" do
    subject(described_module().get_oauth_url)

    let :fake_client_id, do: "fake_client_id"
    let :fake_state, do: "fake_state"
    before do
      allow System |> to(accept :get_env, fn("CLIENT_ID") -> fake_client_id() end)
      allow SlackBlamemta.Token |> to(accept :generate_and_sign, fn -> {:ok, fake_state(), nil} end)
    end

    it "should return the correct auth url with the client_id & state" do
      expect(subject()) |> to(
        eq "https://slack.com/oauth/authorize?client_id=#{fake_client_id()}&scope=commands&state=#{fake_state()}"
      )
    end
  end

  describe "post_message/3" do
    subject do: described_module().post_message(user(), channel_id(), text())

    let :user, do: %User{}
    let :channel_id, do: "fake_channel_id"
    let :text, do: "fake_text"

    context "when the post message fails" do

      let :fake_error, do: "fake_error_msg"

      before do
        allow Slack.Web.Chat |> to(accept :post_message, fn(_a,_b,_c) -> %{"ok" => false, "error" => fake_error()} end)
      end

      it "should return error" do
        {:error, %{errors: error}} = subject()
        expect(error) |> to(eq fake_error())
      end
    end

    context "when the post message suceeds" do

      before do
        allow Slack.Web.Chat |> to(accept :post_message, fn(_a,_b,_c) -> %{"ok" => true} end)
      end

      it "should return ok" do
        expect(subject()) |> to(eq {:ok})
      end
    end
  end

  describe "store_user/1" do
    subject do: described_module().store_user(code())

    let :code, do: 'fake_code'

    context "when the oauth credentials are invalid" do
      let :fake_oauth_error, do: "fake_error_msg"
      before do
        allow Slack.Web.Oauth |> to(accept :access, fn(_a,_b,_c) -> %{"ok" => false, "error" => fake_oauth_error()} end)
      end

      it "should return a error tuple with the error message" do
        {:error, %{errors: error}} = subject()
        expect(error) |> to(eq fake_oauth_error())
      end
    end

    context "When the oauth credentials are valid" do
      let :fake_access_token, do: "fake_access_token"
      before do
        allow Slack.Web.Oauth |> to(accept :access, fn(_a,_b,_c) -> %{"ok" => true, "access_token" => fake_access_token()} end)
      end

      context "and the auth test fails" do
        let :fake_auth_error, do: "fake_auth_error" 

        before do
          allow Slack.Web.Auth |> to(accept :test, fn(_a) -> %{"ok" => false, "error" => fake_auth_error()} end)
        end

        it "should return a error tuple with the auth error" do
          {:error, %{errors: error}} = subject()
          expect(error) |> to(eq fake_auth_error())
        end
      end

      context "and the auth test passes" do

        let :fake_user_id, do: "fake_user_id"
        let :fake_team_id, do: "fake_team_id"

        before do
          allow Slack.Web.Auth |> to(accept :test, fn(_a) -> %{"ok" => true, "user_id" => fake_user_id(), "team_id" => fake_team_id()} end)
          allow SlackBlamemta.Users |> to(accept :create_or_update, fn(_a) ->  {:ok, %User{}} end)
        end

        it "should create a user" do
          subject()
          expect SlackBlamemta.Users |> to(accepted :create_or_update, :any, count: 1)
        end

        it "should return ok and a user" do
          {:ok, user} = subject()
          expect(user) |> to(be_struct User)
        end
      end
    end

  end

end

