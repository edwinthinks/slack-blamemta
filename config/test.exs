use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :slack_blamemta, SlackBlamemtaWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :slack_blamemta, SlackBlamemta.Repo,
  username: "postgres",
  password: "postgres",
  database: "slack_blamemta_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
